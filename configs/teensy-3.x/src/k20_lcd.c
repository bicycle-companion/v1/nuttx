#include <nuttx/config.h>

#include <stdint.h>
#include <stdbool.h>
#include <debug.h>

#include <nuttx/i2c/i2c_master.h>
#include <arch/board/board.h>

#include "up_arch.h"
#include "chip.h"
#include "kinetis.h"
#include "kinetis_i2c.h"
#include "k20_ssd1306_fb.h"
#include "teensy-3x.h"

/************************************************************************************
 * Public Functions
 ************************************************************************************/

int board_lcd_initialize(void)
{
  if (i2c_dev)
    lcd_dev = ssd1306_initialize(i2c_dev, 0);
  return OK;
}

FAR struct lcd_dev_s *board_lcd_getdev(int lcddev)
{
  return lcd_dev;
}
