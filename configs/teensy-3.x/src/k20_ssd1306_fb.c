/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <debug.h>

#include <nuttx/arch.h>
#include <nuttx/video/fb.h>
#include <nuttx/i2c/i2c_master.h>
#include <arch/board/board.h>
#include "teensy-3x.h"
#include "k20_ssd1306_fb.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#define SSD1306_I2C_FREQ 400000
#define SSD1306_I2C_ADDR 60

/****************************************************************************
 * Private Types
 ****************************************************************************/

/* This structure describes the state of the SSD1306 driver */

struct ssd1306_dev_s
{
  struct fb_vtable_s      dev;      /* Publicly visible device structure */

  /* Private LCD-specific information follows */

  FAR struct i2c_master_s  *i2c;      /* Cached I2C device reference */
  uint8_t                  contrast; /* Current contrast setting */
  bool                     on;       /* true: display is on */

  uint8_t fb[SSD1306_DEV_FBSIZE];
};

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

static int ssd1306_fb_getvideoinfo(FAR struct fb_vtable_s *vtable,
             FAR struct fb_videoinfo_s *vinfo);
static int ssd1306_fb_getplaneinfo(FAR struct fb_vtable_s *vtable, int planeno,
             FAR struct fb_planeinfo_s *pinfo);

void ssd1306_fb_sendbyte(FAR struct ssd1306_dev_s *priv, uint8_t regval);
void ssd1306_fb_sendblk(FAR struct ssd1306_dev_s *priv, uint8_t *data, uint8_t len);

void ssd1306_fb_initialize(FAR struct ssd1306_dev_s* priv);
void ssd1306_fb_clear(FAR struct ssd1306_dev_s* priv, int color);
void ssd1306_fb_blit(FAR struct ssd1306_dev_s* priv);

int ssd1306_fb_setcontrast(struct ssd1306_dev_s* priv, unsigned int contrast);
int ssd1306_fb_setpower(struct ssd1306_dev_s* priv, unsigned int contrast);

/****************************************************************************
 * Private Data
 ****************************************************************************/

static struct ssd1306_dev_s g_ssd1306_dev;

/* This structure describes the video controller */

static const struct fb_videoinfo_s g_videoinfo =
{
  .fmt      = SSD1306_DEV_COLORFMT,
  .xres     = SSD1306_DEV_XRES,
  .yres     = SSD1306_DEV_YRES,
  .nplanes  = 1,
};

/* This structure describes the single color plane */

static const struct fb_planeinfo_s g_planeinfo =
{
  .fbmem    = (FAR void *)g_ssd1306_dev.fb,
  .fblen    = SSD1306_DEV_FBSIZE,
  .stride   = SSD1306_DEV_YSTRIDE,
  .display  = 0,
  .bpp      = 1,
};


/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: ssd1306_fb_getvideoinfo
 ****************************************************************************/

static int ssd1306_fb_getvideoinfo(FAR struct fb_vtable_s *vtable,
                              FAR struct fb_videoinfo_s *vinfo)
{
  lcdinfo("vtable=%p vinfo=%p\n", vtable, vinfo);
  if (vtable && vinfo)
    {
      memcpy(vinfo, &g_videoinfo, sizeof(struct fb_videoinfo_s));
      return OK;
    }

  lcderr("ERROR: Returning EINVAL\n");
  return -EINVAL;
}

/****************************************************************************
 * Name: ssd1306_fb_getplaneinfo
 ****************************************************************************/

static int ssd1306_fb_getplaneinfo(FAR struct fb_vtable_s *vtable, int planeno,
                              FAR struct fb_planeinfo_s *pinfo)
{
  lcdinfo("vtable=%p planeno=%d pinfo=%p\n", vtable, planeno, pinfo);
  if (vtable && planeno == 0 && pinfo)
    {
      memcpy(pinfo, &g_planeinfo, sizeof(struct fb_planeinfo_s));
      return OK;
    }

  lcderr("ERROR: Returning EINVAL\n");
  return -EINVAL;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: up_fbinitialize
 *
 * Description:
 *   Initialize the framebuffer video hardware associated with the display.
 *
 * Input parameters:
 *   display - In the case of hardware with multiple displays, this
 *     specifies the display.  Normally this is zero.
 *
 * Returned Value:
 *   Zero is returned on success; a negated errno value is returned on any
 *   failure.
 *
 ****************************************************************************/

int up_fbinitialize(int display)
{
  /* initialize device instance */
  g_ssd1306_dev.i2c = i2c_dev;
  g_ssd1306_dev.dev.getvideoinfo  = ssd1306_fb_getvideoinfo;
  g_ssd1306_dev.dev.getplaneinfo  = ssd1306_fb_getplaneinfo;

  ssd1306_fb_initialize(&g_ssd1306_dev);

  return OK;
}

/****************************************************************************
 * Name: up_fbgetvplane
 *
 * Description:
 *   Return a a reference to the framebuffer object for the specified video
 *   plane of the specified plane.  Many OSDs support multiple planes of video.
 *
 * Input parameters:
 *   display - In the case of hardware with multiple displays, this
 *     specifies the display.  Normally this is zero.
 *   vplane - Identifies the plane being queried.
 *
 * Returned Value:
 *   A non-NULL pointer to the frame buffer access structure is returned on
 *   success; NULL is returned on any failure.
 *
 ****************************************************************************/

FAR struct fb_vtable_s *up_fbgetvplane(int display, int vplane)
{
  return &g_ssd1306_dev.dev;
}

/****************************************************************************
 * Name: up_fbuninitialize
 *
 * Description:
 *   Uninitialize the framebuffer support for the specified display.
 *
 * Input Parameters:
 *   display - In the case of hardware with multiple displays, this
 *     specifies the display.  Normally this is zero.
 *
 * Returned Value:
 *   None
 *
 ****************************************************************************/

void up_fbuninitialize(int display)
{

}

void up_fbredraw(int display)
{
  ssd1306_fb_blit(&g_ssd1306_dev);
}

void up_fbclear(int display)
{
  ssd1306_fb_clear(&g_ssd1306_dev, CONFIG_NX_BGCOLOR);
}

void up_fbsetcontrast(int display, unsigned int contrast)
{
  ssd1306_fb_setcontrast(&g_ssd1306_dev, contrast);
}

void up_fbsetpower(int display, unsigned int power)
{
  ssd1306_fb_setpower(&g_ssd1306_dev, power);
}

/************************************************************************************
 * Name:  ssd1306_lcdclear
 *
 * Description:
 *
 ************************************************************************************/

void ssd1306_fb_clear(FAR struct ssd1306_dev_s* priv, int color)
{
  /* Make an 8-bit version of the selected color */

  if (color & 1)
    {
      color = 0xff;
    }
  else
    {
      color = 0;
    }

  /* Initialize the framebuffer */

  memset(priv->fb, color, SSD1306_DEV_FBSIZE);
}

/************************************************************************************
 * Name:  ssd1306_lcdclear
 *
 * Description:
 *
 ************************************************************************************/

#define SSD1306_MAX_TRANSFER_SIZE 16
#define SSD1306_TRANSFERS 8 // 128 / 16

void ssd1306_fb_blit(FAR struct ssd1306_dev_s* priv)
{
  unsigned int i;

  ssd1306_fb_sendbyte(priv, SSD1306_SET_COLUMN_RANGE);
  ssd1306_fb_sendbyte(priv, 0);
  ssd1306_fb_sendbyte(priv, 127);

  ssd1306_fb_sendbyte(priv, SSD1306_SET_PAGE_RANGE);
  ssd1306_fb_sendbyte(priv, 0);
  ssd1306_fb_sendbyte(priv, 7);

  for (i = 0; i < SSD1306_TRANSFERS; i++)
    ssd1306_fb_sendblk(priv, &priv->fb[i * SSD1306_MAX_TRANSFER_SIZE * SSD1306_DEV_YSTRIDE],
        SSD1306_MAX_TRANSFER_SIZE * SSD1306_DEV_YSTRIDE);
}

void ssd1306_fb_initialize(FAR struct ssd1306_dev_s* priv)
{
  up_mdelay(5);

  /* Configure the device */

  ssd1306_fb_sendbyte(priv, SSD1306_DISPOFF);          /* Display off 0xae */

#if 0
  ssd1306_fb_sendbyte(priv, SSD1306_SETCOLL(0));       /* Set lower column address 0x00 */
  ssd1306_fb_sendbyte(priv, SSD1306_SETCOLH(0));       /* Set higher column address 0x10 */
#endif


  ssd1306_fb_sendbyte(priv, SSD1306_STARTLINE(0));     /* Set display start line 0x40 */

  ssd1306_fb_sendbyte(priv, SSD1306_MEMORYMODE);
  //ssd1306_fb_sendbyte(priv, SSD1306_VERTICAL_MODE);
  //ssd1306_fb_sendbyte(priv, SSD1306_HORIZONTAL_MODE);
  ssd1306_fb_sendbyte(priv, SSD1306_VERTICAL_MODE);

  ssd1306_fb_sendbyte(priv, SSD1306_SET_COLUMN_RANGE);
  ssd1306_fb_sendbyte(priv, 0);
  ssd1306_fb_sendbyte(priv, 127);

  ssd1306_fb_sendbyte(priv, SSD1306_SET_PAGE_RANGE);
  ssd1306_fb_sendbyte(priv, 0);
  ssd1306_fb_sendbyte(priv, 7);

  ssd1306_fb_sendbyte(priv, SSD1306_SCANTOCOM0);


  ssd1306_fb_sendbyte(priv, SSD1306_CONTRAST_MODE);    /* Contrast control 0x81 */
  ssd1306_fb_sendbyte(priv, SSD1306_CONTRAST(SSD1306_MAX_CONTRAST));  /* Max contrast */
  ssd1306_fb_sendbyte(priv, SSD1306_REMAPPLEFT);       /* Set segment remap left 95 to 0 | 0xa1 */
  ssd1306_fb_sendbyte(priv, SSD1306_NORMAL);           /* Normal (un-reversed) display mode 0xa6 */
  ssd1306_fb_sendbyte(priv, SSD1306_MRATIO_MODE);      /* Multiplex ratio 0xa8 */
  ssd1306_fb_sendbyte(priv, SSD1306_MRATIO(SSD1306_DEV_DUTY));  /* Duty = 1/64 or 1/32 */
  ssd1306_fb_sendbyte(priv, SSD1306_DISPOFFS_MODE);    /* Set display offset 0xd3 */
  ssd1306_fb_sendbyte(priv, SSD1306_DISPOFFS(0));
  ssd1306_fb_sendbyte(priv, SSD1306_CLKDIV_SET);       /* Set clock divider 0xd5 */
  ssd1306_fb_sendbyte(priv, SSD1306_CLKDIV(8, 0));     /* 0x80 */

  ssd1306_fb_sendbyte(priv, SSD1306_CHRGPER_SET);      /* Set pre-charge period 0xd9 */
  ssd1306_fb_sendbyte(priv, SSD1306_CHRGPER(0x0f, 1)); /* 0xf1 or 0x22 Enhanced mode */

  ssd1306_fb_sendbyte(priv, SSD1306_CMNPAD_CONFIG);    /* Set common pads / set com pins hardware configuration 0xda */
  ssd1306_fb_sendbyte(priv, SSD1306_CMNPAD(SSD1306_DEV_CMNPAD)); /* 0x12 or 0x02 */

  ssd1306_fb_sendbyte(priv, SSD1306_VCOM_SET);         /* set vcomh 0xdb */
  ssd1306_fb_sendbyte(priv, SSD1306_VCOM(0x40));

  ssd1306_fb_sendbyte(priv, SSD1306_CHRPUMP_SET);      /* Set Charge Pump enable/disable 0x8d ssd1306 */
  ssd1306_fb_sendbyte(priv, SSD1306_CHRPUMP_ON);       /* 0x14 close 0x10 */

  ssd1306_fb_sendbyte(priv, SSD1306_DISPON);           /* Display ON 0xaf */

  /* Clear the display */

  up_mdelay(100);

  ssd1306_fb_clear(priv, CONFIG_NX_BGCOLOR);
  ssd1306_fb_blit(priv);

  priv->contrast = SSD1306_DEV_CONTRAST;
}

int ssd1306_fb_setcontrast(struct ssd1306_dev_s* priv, unsigned int contrast)
{
  /* Verify the contrast value */

  if (contrast > SSD1306_MAX_CONTRAST)
  {
    return -EINVAL;
  }

  printf("setting contrast: %i\n", contrast);

  /* Set the contrast */

  ssd1306_fb_sendbyte(priv, SSD1306_CONTRAST_MODE);      /* Set contrast control register */
  ssd1306_fb_sendbyte(priv, SSD1306_CONTRAST(contrast)); /* Data 1: Set 1 of 256 contrast steps */
  priv->contrast = contrast;

  return OK;
}

int ssd1306_fb_setpower(struct ssd1306_dev_s* priv, unsigned int power)
{
  if (power <= 0)
    {
      /* Turn the display off */

      ssd1306_fb_sendbyte(priv, SSD1306_DISPOFF);
      priv->on = false;
    }
  else
    {
      /* Turn the display on */

      ssd1306_fb_sendbyte(priv, SSD1306_DISPON);
      priv->on = true;
    }

  return OK;
}

/****************************************************************************
 * Name: ssd1306_sendbyte
 *
 * Description:
 *   Write an 8-bit value into SSD1306
 *
 ****************************************************************************/

void ssd1306_fb_sendbyte(FAR struct ssd1306_dev_s *priv, uint8_t regval)
{
  /* 8-bit data read sequence:
   *
   *  Start - I2C_Write_Address - SSD1306_Reg_Address - SSD1306_Write_Data - STOP
   */

  struct i2c_msg_s msg;
  uint8_t txbuffer[2];
  int ret;

#ifdef CONFIG_LCD_SSD1306_REGDEBUG
  _err("-> 0x%02x\n", regval);
#endif

  /* Setup to the data to be transferred.  Two bytes:  The SSD1306 register
   * address followed by one byte of data.
   */

  txbuffer[0]   = 0x00;
  txbuffer[1]   = regval;

  /* Setup 8-bit SSD1306 address write message */

  msg.frequency = SSD1306_I2C_FREQ;        /* I2C frequency */
  msg.addr      = SSD1306_I2C_ADDR;        /* 7-bit address */
  msg.flags     = 0;                       /* Write transaction, beginning with START */
  msg.buffer    = txbuffer;                /* Transfer from this address */
  msg.length    = 2;                       /* Send two bytes following the address
                                            * then STOP */

  /* Perform the transfer */

  ret = I2C_TRANSFER(priv->i2c, &msg, 1);
  if (ret < 0)
    {
      snerr("ERROR: I2C_TRANSFER failed: %d\n", ret);
    }
}

/****************************************************************************
 * Name: ssd1306_sendblk
 *
 * Description:
 *   Write an array of bytes to SSD1306
 *
 ****************************************************************************/

void ssd1306_fb_sendblk(FAR struct ssd1306_dev_s *priv, uint8_t *data, uint8_t len)
{
  struct i2c_msg_s msg[2];
  uint8_t transfer_mode;
  int ret;

  /* 8-bit data read sequence:
   *
   *  Start - I2C_Write_Address - Data transfer select - SSD1306_Write_Data - STOP
   */

  /* Send the SSD1306 register address (with no STOP) */

  transfer_mode    = 0x40;                    /* Select data transfer */

  msg[0].frequency = SSD1306_I2C_FREQ;        /* I2C frequency */
  msg[0].addr      = SSD1306_I2C_ADDR;        /* 7-bit address */
  msg[0].flags     = 0;                       /* Write transaction, beginning with START */
  msg[0].buffer    = &transfer_mode;          /* Transfer mode send */
  msg[0].length    = 1;                       /* Send the one byte register address */

  /* Followed by the SSD1306 write data (with no RESTART, then STOP) */

  msg[1].frequency = SSD1306_I2C_FREQ;        /* I2C frequency */
  msg[1].addr      = SSD1306_I2C_ADDR;        /* 7-bit address */
  msg[1].flags     = I2C_M_NORESTART;         /* Write transaction with no RESTART */
  msg[1].buffer    = data;                    /* Transfer from this address */
  msg[1].length    = len;                     /* Send the data, then STOP */

  ret = I2C_TRANSFER(priv->i2c, msg, 2);
  if (ret < 0)
    {
      snerr("ERROR: I2C_TRANSFER failed: %d\n", ret);
    }
}


