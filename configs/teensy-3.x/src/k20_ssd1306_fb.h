#ifndef __K20_SSD1306_FB_H__
#define __K20_SSD1306_FB_H__

#include <nuttx/config.h>

#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <debug.h>

#include <nuttx/video/fb.h>
#include <arch/board/board.h>

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

#ifndef CONFIG_NX_BGCOLOR
#  define CONFIG_NX_BGCOLOR SSD1306_Y1_BLACK
#endif

/* SSD1306 Commands *******************************************************************/

#define SSD1306_MEMORYMODE       0x20
#define SSD1306_HORIZONTAL_MODE  0x00
#define SSD1306_VERTICAL_MODE    0x01
#define SSD1306_PAGE_MODE        0x02

#define SSD1306_SET_COLUMN_RANGE 0x21
#define SSD1306_SET_PAGE_RANGE   0x22


#define SSD1306_SETCOLL(ad)      (0x00 | ((ad) & 0x0f)) /* Set Lower Column Address: (00h - 0fh) */
#define SSD1306_SETCOLH(ad)      (0x10 | ((ad) & 0x0f)) /* Set Higher Column Address: (10h - 1fh) */
#define SSD1306_STARTLINE(ln)    (0x40 | ((ln) & 0x3f)) /* Set Display Start Line: (40h - 7fh) */
#define SSD1306_CONTRAST_MODE    (0x81)                 /* Set Contrast Control Register: (Double Bytes Command) */
#  define SSD1306_CONTRAST(c)    (c)
#define SSD1306_SEGREMAP(m)      (0xa0 | ((m) & 0x01))  /* Set Segment Re-map: (a0h - a1h) */
#  define SSD1306_REMAPRIGHT     SSD1306_SEGREMAP(0)    /*   Right rotation */
#  define SSD1306_REMAPPLEFT     SSD1306_SEGREMAP(1)    /*   Left rotation */
#define SSD1306_EDISPOFFON(s)    (0xa4 | ((s) & 0x01))  /* Set Entire Display OFF/ON: (a4h - a5h) */
#  define SSD1306_EDISPOFF       SSD1306_EDISPOFFON(0)  /*   Display off */
#  define SSD1306_EDISPON        SSD1306_EDISPOFFON(1)  /*   Display on */
#define SSD1306_NORMREV(s)       (0xa6 | ((s) & 0x01))  /* Set Normal/Reverse Display: (a6h -a7h) */
#  define SSD1306_NORMAL         SSD1306_NORMREV(0)     /*   Normal display */
#  define SSD1306_REVERSE        SSD1306_NORMREV(1)     /*   Reverse display */
#define SSD1306_MRATIO_MODE      (0xa8)                 /* Set Multiplex Ration: (Double Bytes Command) */
#  define SSD1306_MRATIO(d)      ((d) & 0x3f)
#define SSD1306_DCDC_MODE        (0xad)                 /* Set DC-DC OFF/ON: (Double Bytes Command) */
#  define SSD1306_DCDC_OFF       (0x8a)
#  define SSD1306_DCDC_ON        (0x8b)

#define SSD1306_DISPOFFON(s)     (0xae | ((s) & 0x01))  /* Display OFF/ON: (aeh - afh) */
#  define SSD1306_DISPOFF        SSD1306_DISPOFFON(0)   /*   Display off */
#  define SSD1306_DISPON         SSD1306_DISPOFFON(1)   /*   Display on */
#define SSD1306_PAGEADDR(a)      (0xb0 | ((a) & 0x0f))  /* Set Page Address: (b0h - b7h) */
#define SSD1306_SCANDIR(d)       (0xc0 | ((d) & 0x08))  /* Set Common Output Scan Direction: (c0h - c8h) */
#  define SSD1306_SCANFROMCOM0   SSD1306_SCANDIR(0x00)  /*   Scan from COM[0] to COM[n-1]*/
#  define SSD1306_SCANTOCOM0     SSD1306_SCANDIR(0x08)  /*   Scan from COM[n-1] to COM[0] */
#define SSD1306_DISPOFFS_MODE    (0xd3)                 /* Set Display Offset: (Double Bytes Command) */
#  define SSD1306_DISPOFFS(o)    ((o) & 0x3f)
#define SSD1306_CLKDIV_SET       (0xd5)                 /* Set Display Clock Divide Ratio/Oscillator Frequency: (Double Bytes Command) */
#  define SSD1306_CLKDIV(f,d)    ((((f) & 0x0f) << 4) | ((d) & 0x0f))
#define SSD1306_CHRGPER_SET      (0xd9)                 /* Set Dis-charge/Pre-charge Period: (Double Bytes Command) */
#  define SSD1306_CHRGPER(d,p)   ((((d) & 0x0f) << 4) | ((p) & 0x0f))
#define SSD1306_CMNPAD_CONFIG    (0xda)                 /* Set Common pads hardware configuration: (Double Bytes Command) */
#  define SSD1306_CMNPAD(c)      ((0x02) | ((c) & 0x10))
#define SSD1306_VCOM_SET         (0xdb)                 /* Set VCOM Deselect Level: (Double Byte Command) */
#  define SSD1306_VCOM(v)        (v)

#define SSD1306_CHRPUMP_SET      (0x8d)                 /* Charge Pump Setting */
#  define SSD1306_CHRPUMP_ON     (0x14)
#  define SSD1306_CHRPUMP_OFF    (0x10)

#define SSD1306_RMWSTART         (0xe0)                 /* Read-Modify-Write: (e0h) */
#define SSD1306_NOP              (0xe3)                 /* NOP: (e3h) */
#define SSD1306_END              (0xee)                 /* End: (eeh) */

#define SSD1306_WRDATA(d)        (d)                    /* Write Display Data */
#define SSD1306_STATUS_BUSY      (0x80)                 /* Read Status */
#define SSD1306_STATUS_ONOFF     (0x40)
#define SSD1306_RDDATA(d)        (d)                    /* Read Display Data */

/* Color Properties *******************************************************************/
/* Display Resolution
 *
 * The SSD1306 display controller can handle a resolution of 132x64. The UG-2864HSWEG01
 * on the base board is 128x64; the UG-2832HSWEG04 is 128x32.
 */

#define SSD1306_DEV_NATIVE_XRES 128  /* Actual device columns (bits of consecutive bytes) */
#define SSD1306_DEV_NATIVE_YRES 64   /* 8 pages each 8 rows */
#define SSD1306_DEV_XOFFSET     0    /* Offset to logical column 0 */
#define SSD1306_DEV_PAGES       8    /* 8 pages */
#define SSD1306_DEV_CMNPAD      0x12 /* COM configuration */

#define SSD1306_DEV_XRES        SSD1306_DEV_NATIVE_XRES
#define SSD1306_DEV_YRES        SSD1306_DEV_NATIVE_YRES

/* Bytes per logical row and actual device row */

#  define SSD1306_DEV_YSTRIDE     (SSD1306_DEV_YRES >> 3)

#define SSD1306_DEV_DUTY          (SSD1306_DEV_NATIVE_YRES-1)

/* Color depth and format */

#define SSD1306_DEV_BPP           1
#define SSD1306_DEV_COLORFMT      FB_FMT_Y1

/* Default contrast */

#define SSD1306_DEV_CONTRAST      (128)
#define SSD1306_MAX_CONTRAST      256

/* The size of the shadow frame buffer or one row buffer.
 *
 * Frame buffer size: 128 columns x 64 rows / 8 bits-per-pixel
 * Row size:          128 columns x 8 rows-per-page / 8 bits-per-pixel
 */

#define SSD1306_DEV_FBSIZE        (SSD1306_DEV_YSTRIDE * SSD1306_DEV_XRES)
#define SSD1306_DEV_ROWSIZE       (SSD1306_DEV_YSTRIDE)

/************************************************************************************
 * Public Functions
 ************************************************************************************/

#endif
