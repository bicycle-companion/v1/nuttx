#ifndef __ARCH_ARM_SRC_KINETIS_ADC_H
#define __ARCH_ARM_SRC_KINETIS_ADC_H

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/analog/adc.h>
#include "chip/kinetis_adc.h"

#if defined(CONFIG_KINETIS_ADC0) || defined(CONFIG_KINETIS_ADC1)

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#define KINETIS_ADC_MUX_FLAG    (1 << 6)

/****************************************************************************
 * Public Types
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

#ifndef __ASSEMBLY__

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: kinetis_adcinitialize
 *
 * Description:
 *   Initialize the adc
 *
 * Returned Value:
 *   Valid can device structure reference on succcess; a NULL on failure
 *
 ****************************************************************************/

FAR struct adc_dev_s *kinetis_adcinitialize(int port);

#undef EXTERN
#if defined(__cplusplus)
}
#endif

#endif /* __ASSEMBLY__ */
#endif /* defined(CONFIG_KINETIS_ADC0) || defined(CONFIG_KINETIS_ADC1) */
#endif /* __ARCH_ARM_SRC_KINETIS_ADC_H */
