/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <stdio.h>
#include <sys/types.h>
#include <stdint.h>
#include <stdbool.h>
#include <semaphore.h>
#include <errno.h>
#include <assert.h>
#include <debug.h>
#include <nuttx/fs/ioctl.h>

#include <nuttx/irq.h>
#include <nuttx/arch.h>
#include <nuttx/analog/adc.h>

#include "up_internal.h"
#include "up_arch.h"

#include "chip.h"
#include "chip/kinetis_sim.h"
#include "chip/kinetis_pinmux.h"

#include "kinetis_adc.h"

#include <arch/board/board.h>

#if defined(CONFIG_KINETIS_ADC0) || defined(CONFIG_KINETIS_ADC1)

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#if BOARD_BUS_FREQ == 120000000
  #define ADC_CFG1_16BIT  (ADC_CFG1_ADIV_DIV5 + ADC_CFG1_ADICLK_BUSDIV2) // 7.5 MHz
  #define ADC_CFG1_12BIT  (ADC_CFG1_ADIV_DIV4 + ADC_CFG1_ADICLK_BUSDIV2) // 15 MHz
  #define ADC_CFG1_10BIT  (ADC_CFG1_ADIV_DIV4 + ADC_CFG1_ADICLK_BUSDIV2) // 15 MHz
  #define ADC_CFG1_8BIT   (ADC_CFG1_ADIV_DIV4 + ADC_CFG1_ADICLK_BUSDIV2) // 15 MHz
#elif BOARD_BUS_FREQ == 108000000
  #define ADC_CFG1_16BIT  (ADC_CFG1_ADIV_DIV5 + ADC_CFG1_ADICLK_BUSDIV2) // 7 MHz
  #define ADC_CFG1_12BIT  (ADC_CFG1_ADIV_DIV4 + ADC_CFG1_ADICLK_BUSDIV2) // 14 MHz
  #define ADC_CFG1_10BIT  (ADC_CFG1_ADIV_DIV4 + ADC_CFG1_ADICLK_BUSDIV2) // 14 MHz
  #define ADC_CFG1_8BIT   (ADC_CFG1_ADIV_DIV4 + ADC_CFG1_ADICLK_BUSDIV2) // 14 MHz
#elif BOARD_BUS_FREQ == 96000000
  #define ADC_CFG1_16BIT  (ADC_CFG1_ADIV_DIV4 + ADC_CFG1_ADICLK_BUSDIV2) // 12 MHz
  #define ADC_CFG1_12BIT  (ADC_CFG1_ADIV_DIV4 + ADC_CFG1_ADICLK_BUSDIV2) // 12 MHz
  #define ADC_CFG1_10BIT  (ADC_CFG1_ADIV_DIV4 + ADC_CFG1_ADICLK_BUSDIV2) // 12 MHz
  #define ADC_CFG1_8BIT   (ADC_CFG1_ADIV_DIV2 + ADC_CFG1_ADICLK_BUSDIV2) // 24 MHz
#elif BOARD_BUS_FREQ == 90000000
  #define ADC_CFG1_16BIT  (ADC_CFG1_ADIV_DIV4 + ADC_CFG1_ADICLK_BUSDIV2) // 11.25 MHz
  #define ADC_CFG1_12BIT  (ADC_CFG1_ADIV_DIV4 + ADC_CFG1_ADICLK_BUSDIV2) // 11.25 MHz
  #define ADC_CFG1_10BIT  (ADC_CFG1_ADIV_DIV4 + ADC_CFG1_ADICLK_BUSDIV2) // 11.25 MHz
  #define ADC_CFG1_8BIT   (ADC_CFG1_ADIV_DIV2 + ADC_CFG1_ADICLK_BUSDIV2) // 22.5 MHz
#elif BOARD_BUS_FREQ == 80000000
  #define ADC_CFG1_16BIT  (ADC_CFG1_ADIV_DIV4 + ADC_CFG1_ADICLK_BUSDIV2) // 10 MHz
  #define ADC_CFG1_12BIT  (ADC_CFG1_ADIV_DIV4 + ADC_CFG1_ADICLK_BUSDIV2) // 10 MHz
  #define ADC_CFG1_10BIT  (ADC_CFG1_ADIV_DIV4 + ADC_CFG1_ADICLK_BUSDIV2) // 10 MHz
  #define ADC_CFG1_8BIT   (ADC_CFG1_ADIV_DIV2 + ADC_CFG1_ADICLK_BUSDIV2) // 20 MHz
#elif BOARD_BUS_FREQ == 72000000
  #define ADC_CFG1_16BIT  (ADC_CFG1_ADIV_DIV4 + ADC_CFG1_ADICLK_BUSDIV2) // 9 MHz
  #define ADC_CFG1_12BIT  (ADC_CFG1_ADIV_DIV2 + ADC_CFG1_ADICLK_BUSDIV2) // 18 MHz
  #define ADC_CFG1_10BIT  (ADC_CFG1_ADIV_DIV2 + ADC_CFG1_ADICLK_BUSDIV2) // 18 MHz
  #define ADC_CFG1_8BIT   (ADC_CFG1_ADIV_DIV2 + ADC_CFG1_ADICLK_BUSDIV2) // 18 MHz
#elif BOARD_BUS_FREQ == 64000000
  #define ADC_CFG1_16BIT  (ADC_CFG1_ADIV_DIV4 + ADC_CFG1_ADICLK_BUSDIV2) // 8 MHz
  #define ADC_CFG1_12BIT  (ADC_CFG1_ADIV_DIV2 + ADC_CFG1_ADICLK_BUSDIV2) // 16 MHz
  #define ADC_CFG1_10BIT  (ADC_CFG1_ADIV_DIV2 + ADC_CFG1_ADICLK_BUSDIV2) // 16 MHz
  #define ADC_CFG1_8BIT   (ADC_CFG1_ADIV_DIV2 + ADC_CFG1_ADICLK_BUSDIV2) // 16 MHz
#elif BOARD_BUS_FREQ == 60000000
  #define ADC_CFG1_16BIT  (ADC_CFG1_ADIV_DIV4 + ADC_CFG1_ADICLK_BUSDIV2) // 7.5 MHz
  #define ADC_CFG1_12BIT  (ADC_CFG1_ADIV_DIV2 + ADC_CFG1_ADICLK_BUSDIV2) // 15 MHz
  #define ADC_CFG1_10BIT  (ADC_CFG1_ADIV_DIV2 + ADC_CFG1_ADICLK_BUSDIV2) // 15 MHz
  #define ADC_CFG1_8BIT   (ADC_CFG1_ADIV_DIV2 + ADC_CFG1_ADICLK_BUSDIV2) // 15 MHz
#elif BOARD_BUS_FREQ == 56000000 || BOARD_BUS_FREQ == 54000000
  #define ADC_CFG1_16BIT  (ADC_CFG1_ADIV_DIV4 + ADC_CFG1_ADICLK_BUSDIV2) // 7 MHz
  #define ADC_CFG1_12BIT  (ADC_CFG1_ADIV_DIV2 + ADC_CFG1_ADICLK_BUSDIV2) // 14 MHz
  #define ADC_CFG1_10BIT  (ADC_CFG1_ADIV_DIV2 + ADC_CFG1_ADICLK_BUSDIV2) // 14 MHz
  #define ADC_CFG1_8BIT   (ADC_CFG1_ADIV_DIV2 + ADC_CFG1_ADICLK_BUSDIV2) // 14 MHz
#elif BOARD_BUS_FREQ == 48000000
  #define ADC_CFG1_16BIT  (ADC_CFG1_ADIV_DIV2 + ADC_CFG1_ADICLK_BUSDIV2) // 12 MHz
  #define ADC_CFG1_12BIT  (ADC_CFG1_ADIV_DIV2 + ADC_CFG1_ADICLK_BUSDIV2) // 12 MHz
  #define ADC_CFG1_10BIT  (ADC_CFG1_ADIV_DIV2 + ADC_CFG1_ADICLK_BUSDIV2) // 12 MHz
  #define ADC_CFG1_8BIT   (ADC_CFG1_ADIV_DIV1 + ADC_CFG1_ADICLK_BUSDIV2) // 24 MHz
#elif BOARD_BUS_FREQ == 40000000
  #define ADC_CFG1_16BIT  (ADC_CFG1_ADIV_DIV2 + ADC_CFG1_ADICLK_BUSDIV2) // 10 MHz
  #define ADC_CFG1_12BIT  (ADC_CFG1_ADIV_DIV2 + ADC_CFG1_ADICLK_BUSDIV2) // 10 MHz
  #define ADC_CFG1_10BIT  (ADC_CFG1_ADIV_DIV2 + ADC_CFG1_ADICLK_BUSDIV2) // 10 MHz
  #define ADC_CFG1_8BIT   (ADC_CFG1_ADIV_DIV1 + ADC_CFG1_ADICLK_BUSDIV2) // 20 MHz
#elif BOARD_BUS_FREQ == 36000000
  #define ADC_CFG1_16BIT  (ADC_CFG1_ADIV_DIV2 + ADC_CFG1_ADICLK_BUSDIV2) // 9 MHz
  #define ADC_CFG1_12BIT  (ADC_CFG1_ADIV_DIV1 + ADC_CFG1_ADICLK_BUSDIV2) // 18 MHz
  #define ADC_CFG1_10BIT  (ADC_CFG1_ADIV_DIV1 + ADC_CFG1_ADICLK_BUSDIV2) // 18 MHz
  #define ADC_CFG1_8BIT   (ADC_CFG1_ADIV_DIV1 + ADC_CFG1_ADICLK_BUSDIV2) // 18 MHz
#elif BOARD_BUS_FREQ == 24000000
  #define ADC_CFG1_16BIT  (ADC_CFG1_ADIV_DIV2 + ADC_CFG1_ADICLK_BUSCLK) // 12 MHz
  #define ADC_CFG1_12BIT  (ADC_CFG1_ADIV_DIV2 + ADC_CFG1_ADICLK_BUSCLK) // 12 MHz
  #define ADC_CFG1_10BIT  (ADC_CFG1_ADIV_DIV2 + ADC_CFG1_ADICLK_BUSCLK) // 12 MHz
  #define ADC_CFG1_8BIT   (ADC_CFG1_ADIV_DIV1 + ADC_CFG1_ADICLK_BUSCLK) // 24 MHz
#elif BOARD_BUS_FREQ == 16000000
  #define ADC_CFG1_16BIT  (ADC_CFG1_ADIV_DIV1 + ADC_CFG1_ADICLK_BUSCLK) // 8 MHz
  #define ADC_CFG1_12BIT  (ADC_CFG1_ADIV_DIV1 + ADC_CFG1_ADICLK_BUSCLK) // 8 MHz
  #define ADC_CFG1_10BIT  (ADC_CFG1_ADIV_DIV1 + ADC_CFG1_ADICLK_BUSCLK) // 8 MHz
  #define ADC_CFG1_8BIT   (ADC_CFG1_ADIV_DIV1 + ADC_CFG1_ADICLK_BUSCLK) // 16 MHz
#elif BOARD_BUS_FREQ == 8000000
  #define ADC_CFG1_16BIT  (ADC_CFG1_ADIV_DIV1 + ADC_CFG1_ADICLK_BUSCLK) // 8 MHz
  #define ADC_CFG1_12BIT  (ADC_CFG1_ADIV_DIV1 + ADC_CFG1_ADICLK_BUSCLK) // 8 MHz
  #define ADC_CFG1_10BIT  (ADC_CFG1_ADIV_DIV1 + ADC_CFG1_ADICLK_BUSCLK) // 8 MHz
  #define ADC_CFG1_8BIT   (ADC_CFG1_ADIV_DIV1 + ADC_CFG1_ADICLK_BUSCLK) // 8 MHz
#elif BOARD_BUS_FREQ == 4000000
  #define ADC_CFG1_16BIT  (ADC_CFG1_ADIV_DIV1 + ADC_CFG1_ADICLK_BUSCLK) // 4 MHz
  #define ADC_CFG1_12BIT  (ADC_CFG1_ADIV_DIV1 + ADC_CFG1_ADICLK_BUSCLK) // 4 MHz
  #define ADC_CFG1_10BIT  (ADC_CFG1_ADIV_DIV1 + ADC_CFG1_ADICLK_BUSCLK) // 4 MHz
  #define ADC_CFG1_8BIT   (ADC_CFG1_ADIV_DIV1 + ADC_CFG1_ADICLK_BUSCLK) // 4 MHz
#elif BOARD_BUS_FREQ == 2000000
  #define ADC_CFG1_16BIT  (ADC_CFG1_ADIV_DIV1 + ADC_CFG1_ADICLK_BUSCLK) // 2 MHz
  #define ADC_CFG1_12BIT  (ADC_CFG1_ADIV_DIV1 + ADC_CFG1_ADICLK_BUSCLK) // 2 MHz
  #define ADC_CFG1_10BIT  (ADC_CFG1_ADIV_DIV1 + ADC_CFG1_ADICLK_BUSCLK) // 2 MHz
  #define ADC_CFG1_8BIT   (ADC_CFG1_ADIV_DIV1 + ADC_CFG1_ADICLK_BUSCLK) // 2 MHz
#else
#error "BOARD_BUS_FREQ must be 120, 108, 96, 90, 80, 72, 64, 60, 56, 54, 48, 40, 36, 24, 4 or 2 MHz"
#endif

/****************************************************************************
 * Private Types
 ****************************************************************************/

struct up_dev_s
{
  FAR const struct adc_callback_s *cb;
  int         irq;
  uint32_t    base;
  bool        busy;
};

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/* ADC methods */

static int  adc_bind(FAR struct adc_dev_s *dev,
                     FAR const struct adc_callback_s *callback);
static void adc_reset(FAR struct adc_dev_s *dev);
static int  adc_setup(FAR struct adc_dev_s *dev);
static void adc_shutdown(FAR struct adc_dev_s *dev);
static void adc_rxint(FAR struct adc_dev_s *dev, bool enable);
static int  adc_ioctl(FAR struct adc_dev_s *dev, int cmd, unsigned long arg);
static int  adc_interrupt(int irq, void *context);

static int  adc_trigger(FAR struct up_dev_s *priv, unsigned long channel);

/****************************************************************************
 * Private Data
 ****************************************************************************/

static const struct adc_ops_s g_adcops =
{
  .ao_bind     = adc_bind,
  .ao_reset    = adc_reset,
  .ao_setup    = adc_setup,
  .ao_shutdown = adc_shutdown,
  .ao_rxint    = adc_rxint,
  .ao_ioctl    = adc_ioctl,
};


#ifdef CONFIG_KINETIS_ADC0
static struct adc_dev_s g_adcdev0;
static struct up_dev_s g_adcpriv0;
#endif

#ifdef CONFIG_KINETIS_ADC1
static struct adc_dev_s g_adcdev1;
static struct up_dev_s g_adcpriv1;
#endif

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: adc_bind
 *
 * Description:
 *   Bind the upper-half driver callbacks to the lower-half implementation.  This
 *   must be called early in order to receive ADC event notifications.
 *
 ****************************************************************************/

static int adc_bind(FAR struct adc_dev_s *dev,
                    FAR const struct adc_callback_s *callback)
{
  FAR struct up_dev_s *priv = (FAR struct up_dev_s *)dev->ad_priv;

  _info("bind\n");

  DEBUGASSERT(priv != NULL);
  priv->cb = callback;
  return OK;
}

/****************************************************************************
 * Name: adc_reset
 *
 * Description:
 *   Reset the ADC device.  Called early to initialize the hardware. This
 *   is called, before adc_setup() and on error conditions.
 *
 ****************************************************************************/

static void adc_reset(FAR struct adc_dev_s *dev)
{
  /** setup here, pins **/
}

/****************************************************************************
 * Name: adc_setup
 *
 * Description:
 *   Configure the ADC. This method is called the first time that the ADC
 *   device is opened.  This will occur when the port is first opened.
 *   This setup includes configuring and attaching ADC interrupts.  Interrupts
 *   are all disabled upon return.
 *
 ****************************************************************************/

static int adc_setup(FAR struct adc_dev_s *dev)
{
  FAR struct up_dev_s *priv = (FAR struct up_dev_s *)dev->ad_priv;

  int ret = irq_attach(priv->irq, adc_interrupt);
  if (ret == OK)
    {
      up_enable_irq(priv->irq);
    }

  _info("setup\n");

  return ret;
}

/****************************************************************************
 * Name: adc_shutdown
 *
 * Description:
 *   Disable the ADC.  This method is called when the ADC device is closed.
 *   This method reverses the operation the setup method.
 *
 ****************************************************************************/

static void adc_shutdown(FAR struct adc_dev_s *dev)
{
  FAR struct up_dev_s *priv = (FAR struct up_dev_s *)dev->ad_priv;
  uint32_t regval;

  regval = getreg32(priv->base + KINETIS_ADC_SC1A_OFFSET);
  regval &= ~ADC_SC1_AIEN;
  putreg32(regval, priv->base + KINETIS_ADC_SC1A_OFFSET);

  up_disable_irq(priv->irq);

  /* Then detach the ADC interrupt handler. */

  irq_detach(priv->irq);
}

/****************************************************************************
 * Name: adc_rxint
 *
 * Description:
 *   Call to enable or disable RX interrupts
 *
 ****************************************************************************/

static void adc_rxint(FAR struct adc_dev_s *dev, bool enable)
{
  FAR struct up_dev_s *priv = (FAR struct up_dev_s *)dev->ad_priv;
  uint32_t regval;

  regval = getreg32(priv->base + KINETIS_ADC_SC1A_OFFSET);

  if (enable)
  {
    regval |= ADC_SC1_AIEN;
  }
  else
  {
    regval &= ~ADC_SC1_AIEN;
  }

  putreg32(regval, priv->base + KINETIS_ADC_SC1A_OFFSET);

  _info("RXINT %i\n", enable);
}

/****************************************************************************
 * Name: adc_ioctl
 *
 * Description:
 *  All ioctl calls will be routed through this method
 *
 ****************************************************************************/

static int adc_ioctl(FAR struct adc_dev_s *dev, int cmd, unsigned long arg)
{
  FAR struct up_dev_s *priv = (FAR struct up_dev_s *)dev->ad_priv;
  int ret = OK;

  switch(cmd)
  {
    case ANIOC_TRIGGER:
      ret = adc_trigger(priv, arg);
    break;
    default:
      return -ENOTTY;
    break;
  }

  return ret;
}

/****************************************************************************
 * Name: adc_trigger
 *
 * Description:
 *  Initiate ADC conversion of selected channel
 *
 ****************************************************************************/

static int  adc_trigger(FAR struct up_dev_s *priv, unsigned long channel)
{
  uint32_t regval;
  irqstate_t flags;
  bool mux;
  unsigned long real_channel;

  /* detect mux flag and obtain actual channel number */
  mux = channel & KINETIS_ADC_MUX_FLAG;
  real_channel = channel & ADC_SC1_ADCH_MASK;

  if (real_channel >= ADC_SC1_ADCH_DISABLED)
    {
      _err("Invalid ADC channel input %i\n", channel);
      return -ENODEV;
    }

  if (priv->busy)
    {
      _err("ADC is busy\n");
      return -ENAVAIL;
    }

  _info("Triggering %i (muxed %i)\n", real_channel, (bool)mux);

  flags = enter_critical_section();

  /* select mux */

  regval = getreg32(priv->base + KINETIS_ADC_CFG2_OFFSET);

  if (mux)
    {
      regval |= ADC_CFG2_MUXSEL;
    }
  else
    {
      regval &= ~ADC_CFG2_MUXSEL;
    }

  putreg32(regval, priv->base + KINETIS_ADC_CFG2_OFFSET);

  /* select channel */

  regval = getreg32(priv->base + KINETIS_ADC_SC1A_OFFSET);
  regval = (regval & ~ADC_SC1_ADCH_DISABLED) | real_channel;
  putreg32(regval, priv->base + KINETIS_ADC_SC1A_OFFSET);

  priv->busy = true;

  leave_critical_section(flags);

  return OK;
}

/****************************************************************************
 * Name: adc_interrupt
 *
 * Description:
 *   ADC interrupt handler
 *
 ****************************************************************************/

static int adc_interrupt(int irq, void *context)
{
  _info("ADC ISR\n");
  FAR struct up_dev_s *priv;
  FAR struct adc_dev_s *dev;
  uint32_t value, channel;

#if CONFIG_KINETIS_ADC0
  if (irq == KINETIS_IRQ_ADC0)
    {
      dev = &g_adcdev0;
    }
  else
#endif
#if CONFIG_KINETIS_ADC1
  if (irq == KINETIS_IRQ_ADC1)
    {
      dev = &g_adcdev1;
    }
  else
#endif
    {
      PANIC();
    }

  priv = dev->ad_priv;

  if (priv->cb)
  {
    value = getreg32(priv->base + KINETIS_ADC_RA_OFFSET);
    channel = (getreg32(priv->base + KINETIS_ADC_SC1A_OFFSET) & ADC_SC1_ADCH_MASK);

    if (getreg32(priv->base + KINETIS_ADC_CFG2_OFFSET) & ADC_CFG2_MUXSEL)
    {
      channel |= KINETIS_ADC_MUX_FLAG;
    }

    _info("received %i muxed: %i\n", channel & ADC_SC1_ADCH_MASK, (bool)(channel & KINETIS_ADC_MUX_FLAG));
    priv->cb->au_receive(dev, channel, value);
  }

  priv->busy = false;

  return OK;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: kinetis_adcinitialize
 *
 * Description:
 *   Initialize the adc
 *
 * Returned Value:
 *   Valid can device structure reference on success; a NULL on failure
 *
 ****************************************************************************/

FAR struct adc_dev_s *kinetis_adcinitialize(int port)
{
  FAR struct up_dev_s *priv;
  FAR struct adc_dev_s *dev;
  int32_t regval;
  irqstate_t flags;
  uint16_t sum;

#if defined(CONFIG_KINETIS_ADC0)
  if (port == 0)
    {
      priv = &g_adcpriv0;
      dev = &g_adcdev0;

      priv->irq = KINETIS_IRQ_ADC0;
      priv->base = KINETIS_ADC0_BASE;

      /* enable clock */
      regval = getreg32(KINETIS_SIM_SCGC6);
      regval |= SIM_SCGC6_ADC0;
      putreg32(regval, KINETIS_SIM_SCGC6);
    }
  else
#endif
#if defined(CONFIG_KINETIS_ADC1)
  if (port == 1)
    {
      priv = &g_adcpriv1;
      dev = &g_adcdev1;

      priv->irq = KINETIS_IRQ_ADC1;
      priv->base = KINETIS_ADC1_BASE;

      /* enable clock */
      regval = getreg32(KINETIS_SIM_SCGC3);
      regval |= SIM_SCGC3_ADC1;
      putreg32(regval, KINETIS_SIM_SCGC3);
    }
  else
#endif
    {
      _err("Kinetis ADC%i not supported\n", port);
      return NULL;
    }

  /* setup pointers */
  dev->ad_ops = &g_adcops;
  dev->ad_priv = priv;

  priv->busy = false;

  /* set default configuration for 16bit precision */
  putreg32(ADC_CFG1_16BIT | ADC_CFG1_MODE_1616BIT | ADC_CFG1_ADLSMP, priv->base + KINETIS_ADC_CFG1_OFFSET);
  putreg32(ADC_CFG2_ADLSTS_PLUS6, priv->base + KINETIS_ADC_CFG2_OFFSET);

  _info("calibrating...\n");

  /* calibrate ADC */
  putreg32(ADC_SC3_CAL | ADC_SC3_AVGE | ADC_SC3_AVGS_32SMPLS, priv->base + KINETIS_ADC_SC3_OFFSET);

  while (getreg32(priv->base + KINETIS_ADC_SC3_OFFSET) & ADC_SC3_CAL);

  flags = enter_critical_section();

  /* plus side */
  sum = getreg32(priv->base + KINETIS_ADC_CLPS_OFFSET) +
        getreg32(priv->base + KINETIS_ADC_CLP4_OFFSET) +
        getreg32(priv->base + KINETIS_ADC_CLP3_OFFSET) +
        getreg32(priv->base + KINETIS_ADC_CLP2_OFFSET) +
        getreg32(priv->base + KINETIS_ADC_CLP1_OFFSET) +
        getreg32(priv->base + KINETIS_ADC_CLP0_OFFSET);

  sum = (sum / 2) | 0x8000;

  putreg32(sum, priv->base + KINETIS_ADC_PG_OFFSET);

  /* minus side */
  sum = getreg32(priv->base + KINETIS_ADC_CLMS_OFFSET) +
        getreg32(priv->base + KINETIS_ADC_CLM4_OFFSET) +
        getreg32(priv->base + KINETIS_ADC_CLM3_OFFSET) +
        getreg32(priv->base + KINETIS_ADC_CLM2_OFFSET) +
        getreg32(priv->base + KINETIS_ADC_CLM1_OFFSET) +
        getreg32(priv->base + KINETIS_ADC_CLM0_OFFSET);

  sum = (sum / 2) | 0x8000;

  putreg32(sum, priv->base + KINETIS_ADC_MG_OFFSET);

  leave_critical_section(flags);

  _info("calibration (minus) %i\n", sum);

  return dev;
}

#endif /* CONFIG_LPC43_ADC0 */
